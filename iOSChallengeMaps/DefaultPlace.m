//
//  DefaultPlace.m
//  iOSChallengeMaps
//
//  Created by FUTAPP on 14/7/16.
//  Copyright © 2016 futapp. All rights reserved.
//

#import "DefaultPlace.h"

@implementation DefaultPlace

/**
 *  parse NSDictionary to Place
 *
 *  @param dictionary
 */
- (void)parsePlaceFromNSDictionary:(NSDictionary*) dictionary{
    //read values
    NSString *stringLatitude = [[[dictionary objectForKey:@"geometry"] objectForKey:@"location"] valueForKey:@"lat"];
    NSString *stringLongitude = [[[dictionary objectForKey:@"geometry"] objectForKey:@"location"] valueForKey:@"lng"];
    
    //assign values
    self.formatedAddress = [dictionary objectForKey:@"formatted_address"];
    self.latitude =  [stringLatitude doubleValue];
    self.longitude = [stringLongitude doubleValue];
    
    
}

@end
