//
//  MapViewController.h
//  iOSChallengeMaps
//
//  Created by FUTAPP on 13/7/16.
//  Copyright © 2016 futapp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Place.h"
#import "DefaultPlace.h"
#import <MagicalRecord/MagicalRecord.h>

@interface MapViewController : UIViewController

@property(nonatomic,strong) DefaultPlace* mainPlace;
@property(nonatomic,strong) Place* savePlace;
@property(nonatomic,strong) NSMutableArray* selectedPlaces;
@property(nonatomic) BOOL isUniquePlaceSelected;
@property(nonatomic,strong) NSManagedObjectContext* context;
@property(nonatomic,strong) UIBarButtonItem* saveButton;
@property(nonatomic,strong) UIBarButtonItem* deleteButton;
@property(nonatomic) BOOL isPlaceAlreadyExists;
@property(nonatomic,strong) UIAlertController* alertController;

@end
