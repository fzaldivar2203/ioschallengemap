#import "Place.h"

@interface Place ()

// Private interface goes here.

@end

@implementation Place

/**
 *  parse NSDictionary to Place
 *
 *  @param dictionary
 */
- (void)parsePlaceFromDefaultPlace:(DefaultPlace*) defaultPlace{
    
    //assign values
    self.formatedAddress = defaultPlace.formatedAddress;
    self.latitude =  [NSNumber numberWithDouble:defaultPlace.latitude];
    self.longitude = [NSNumber numberWithDouble:defaultPlace.longitude];
    
    
}

@end
