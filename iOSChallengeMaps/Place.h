#import "_Place.h"
#import "DefaultPlace.h"

@interface Place : _Place
// Custom logic goes here.
- (void)parsePlaceFromDefaultPlace:(DefaultPlace*) defaultPlace;

@end
