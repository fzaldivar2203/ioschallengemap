//  
//  DataManager.m
//  iOSChallengeMaps
//
//  Created by FUTAPP on 13/7/16.
//  Copyright © 2016 futapp. All rights reserved.
//

#import "DataManager.h"
#import "AFNetworking.h"
#import <MagicalRecord/MagicalRecord.h>
#import "Place.h"

@implementation DataManager

#pragma mark -
#pragma mark Constants

NSString *const DataManagerURL = @"https://maps.googleapis.com/maps/api/geocode/json?address=%@&sensor=false";
NSString *const DataManagerResults = @"results";

#pragma mark -
#pragma mark Singleton method and initialization
/**
 *  singleton
 *
 *  @return only instance of Data Manager
 */
+ (instancetype)shareDataManager{
    static dispatch_once_t onceToken;
    static DataManager *shared;
    dispatch_once(&onceToken, ^{
        shared = [DataManager new];
    });
    return  shared;
}

#pragma mark -
#pragma mark Public methods

/**
 *  load all places
 *
 *  @return places
 */
- (void)loadPlaces: (NSString*) text{
    
    __weak typeof(self) weakSelf = self;
    
    NSCharacterSet *set = [NSCharacterSet URLQueryAllowedCharacterSet];
    NSString *encodedUrl = [[NSString stringWithFormat:DataManagerURL ,text]stringByAddingPercentEncodingWithAllowedCharacters:  set];
    
    AFHTTPSessionManager *manager = [self prepareManager];
    [manager GET:encodedUrl
      parameters:nil
        progress:nil
         success:^(NSURLSessionTask *task, id responseObject) {
             
             NSMutableArray* places = [responseObject objectForKey:DataManagerResults];
             
             [weakSelf.delegate readPlaces:places];
         } failure:^(NSURLSessionTask *operation, NSError *error) {
             [weakSelf.delegate readPlaces:nil];
         }];
}

/**
 *  Check if place already exists in database
 *
 *  @param defaultPlace
 *
 *  @return true if already exist
 */
-(BOOL)isPlaceAlreadyInDataBase: (DefaultPlace*) defaultPlace{
    NSMutableArray* places = [[Place MR_findAll] mutableCopy];
    for(Place* currentPlace in places){
        if([currentPlace.formatedAddress isEqualToString:defaultPlace.formatedAddress]){
            return YES;
        }
    }
    return NO;
}

/**
 *  Search place in database and return it
 *
 *  @param defaultPlace
 *
 *  @return place
 */
-(Place*)searchPlace:(DefaultPlace*) defaultPlace{
    NSMutableArray* places = [[Place MR_findAll] mutableCopy];
    for(Place* currentPlace in places){
        if([currentPlace.formatedAddress isEqualToString:defaultPlace.formatedAddress]){
            return currentPlace;
        }
    }
    return nil;
}

#pragma mark -
#pragma mark Private methods

/**
 *  prepare AFHTTPSessionManager
 *
 *  @return AFHTTPSessionManager 
 */
-(AFHTTPSessionManager *) prepareManager{
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    AFJSONRequestSerializer *requestSerializer = [AFJSONRequestSerializer serializer];
    [requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    manager.requestSerializer = requestSerializer;
    manager.responseSerializer =[AFJSONResponseSerializer serializer];
    return manager;
}

@end
