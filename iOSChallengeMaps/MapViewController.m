//
//  MapViewController.m
//  iOSChallengeMaps
//
//  Created by FUTAPP on 13/7/16.
//  Copyright © 2016 futapp. All rights reserved.
//

#import "MapViewController.h"
#import "DefaultPlace.h"
#import "DataManager.h"
@import GoogleMaps;



@interface MapViewController ()

@end

@implementation MapViewController

#pragma mark -
#pragma mark Constant
int const MapViewControllerZoom = 12;
int const MapViewControllerNoZoom = 1;
NSString* MapViewControllerSnippetText =@"(%lf,%lf)";
NSString* MapViewControllerSaveButtonTitle =@"Save";
NSString* MapViewControllerDeleteButtonTitle =@"Delete";
NSString* MapViewControllerAlertTitle =@"Delete Place";
NSString* MapViewControllerAlertMessage =@"Do you really want to delete this place?";
NSString* MapViewControllerAlertButtonCancelText =@"Cancel";
NSString* MapViewControllerAlertButtonDeleteText =@"Delete";

#pragma mark -
#pragma mark Superclass overrides
- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    [self initialize];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
#pragma mark initialize methods
/**
 *  Initialize all objects for the view controller
 */
-(void) initialize{
    
    if(self.isUniquePlaceSelected){
        [self initializeAlertViewController];
        self.context =[NSManagedObjectContext MR_context];
        [self initializeSinglePlace];
    }else{
        [self initializeMultiplePlaces];
    }
}

/**
 *  initialize when only one place is selected
 */
- (void)initializeSinglePlace{
    
    //save button
    self.saveButton = [[UIBarButtonItem alloc]initWithTitle:MapViewControllerSaveButtonTitle style:UIBarButtonItemStylePlain target:self action:@selector(saveMainPlace)];
    
    //delete button
    self.deleteButton = [[UIBarButtonItem alloc]initWithTitle:MapViewControllerDeleteButtonTitle style:UIBarButtonItemStylePlain target:self action:@selector(deleteMainPlace)];
    
    [self setRightBarButton];

    
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:self.mainPlace.latitude
                                                            longitude:self.mainPlace.longitude
                                                                 zoom:MapViewControllerZoom];
    GMSMapView *mapView = [GMSMapView mapWithFrame:CGRectZero camera:camera];
    
    GMSMarker *marker = [[GMSMarker alloc] init];
    marker.position = camera.target;
    
    marker.title = self.mainPlace.formatedAddress;
    marker.snippet = [self snippetText:self.mainPlace];
    marker.appearAnimation = kGMSMarkerAnimationPop;
    marker.map = mapView;
    
    self.view = mapView;
}

/**
 *  initialize when there are several places selected
 */
- (void)initializeMultiplePlaces{
    
    // This place is use to set default latitude and longitude of the camera
    DefaultPlace* firstPlace = [self.selectedPlaces objectAtIndex:0];
    
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:firstPlace.latitude
                                                            longitude:firstPlace.longitude
                                                                 zoom:MapViewControllerNoZoom];
    GMSMapView *mapView = [GMSMapView mapWithFrame:CGRectZero camera:camera];
    
    //create all markers
    for(DefaultPlace* place in self.selectedPlaces){
        CLLocationCoordinate2D position = { place.latitude ,place.longitude};
        GMSMarker *marker = [GMSMarker markerWithPosition:position];
        marker.title = place.formatedAddress;
        marker.snippet = [self snippetText:place];
        marker.appearAnimation = kGMSMarkerAnimationPop;
        marker.map = mapView;
    }
    
    self.view = mapView;
    
}

/**
 *  Initiliaze alert for delete place
 */
-(void)initializeAlertViewController{
    __weak MapViewController* weakSelf = self;
    
    //create alertController
    self.alertController = [UIAlertController alertControllerWithTitle:MapViewControllerAlertTitle  message:MapViewControllerAlertMessage preferredStyle:UIAlertControllerStyleAlert];
    
    //create actions for alertController
    
    [self.alertController addAction:[UIAlertAction actionWithTitle:MapViewControllerAlertButtonDeleteText  style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            weakSelf.savePlace =[[DataManager shareDataManager] searchPlace:self.mainPlace];
            [weakSelf.savePlace MR_deleteEntityInContext:self.context];
            [weakSelf saveContext];
    }]];
    
    [self.alertController addAction:[UIAlertAction actionWithTitle:MapViewControllerAlertButtonCancelText style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [weakSelf dismissViewControllerAnimated:YES completion:nil];
    }]];
}



#pragma mark -
#pragma mark private methods

/**
 *  prepare the text for snippets
 *
 *  @param place
 *
 *  @return text for snippets
 */
- (NSString*) snippetText:(DefaultPlace*) place{
    return [NSString stringWithFormat:MapViewControllerSnippetText,place.latitude,place.longitude];
}

/**
 *  save place
 */
- (void)saveMainPlace{
    self.savePlace =[Place MR_createEntityInContext:self.context];
    [self.savePlace parsePlaceFromDefaultPlace:self.mainPlace];
    [self saveContext];
}

/**
 *  delete place
 */
-(void)deleteMainPlace{
    [self presentViewController:self.alertController animated:YES completion:nil];
}

/**
 *  save context
 */
-(void)saveContext{
    __weak MapViewController* weakSelf = self;
    [self.context MR_saveToPersistentStoreWithCompletion:^(BOOL contextDidSave, NSError *error) {
        if(contextDidSave){
            weakSelf.isPlaceAlreadyExists = !weakSelf.isPlaceAlreadyExists;
            [weakSelf setRightBarButton];
        }else{
            if(error){
                NSLog(@"%@",error.description);
            }
        }
    }];
}

//set rightBarButton
- (void)setRightBarButton{
    if(self.isPlaceAlreadyExists){
        self.navigationItem.rightBarButtonItem =  self.deleteButton;
    }else{
        self.navigationItem.rightBarButtonItem =  self.saveButton;
    }
}


@end
