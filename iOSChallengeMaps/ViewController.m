//
//  ViewController.m
//  iOSChallengeMaps
//
//  Created by FUTAPP on 13/7/16.
//  Copyright © 2016 futapp. All rights reserved.
//

#import "ViewController.h"
#import "DataManager.h"
#import "MapViewController.h"
#import "DefaultPlace.h"


@interface ViewController ()<DataManagerProtocol>

@end

@implementation ViewController

#pragma mark -
#pragma mark Constant
int const ViewControllerTableNumberOfSections =1 ;
float const ViewControllerVisibleDisplayAll = 88.0f;
float const ViewControllerNotVisibleDisplayAll = 0.0f;
NSString* const ViewControllerDisplayAllTableTextWithResults = @"Display all on Map";
NSString* const ViewControllerDisplayAllTableTextWithOutResults = @"No Results";
NSString* const ViewControllerDisplayAllTableCell = @"CELL_DISPLAY_ALL";
NSString* const ViewControllerPlacesCell = @"CELL_PLACES";
NSString* const ViewControllerMapViewControllerIdentifier = @"MapViewController";

#pragma mark -
#pragma mark Superclass overrides

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initialize];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated{
    [self.navigationController setNavigationBarHidden:YES animated:NO];
}

#pragma mark -
#pragma mark initialize methods
/**
 *  Initialize all objects for the view controller
 */
- (void)initialize{
    self.searchBar.delegate = self;
    [DataManager shareDataManager].delegate = self;
    self.displayAllTableArray =  @[ViewControllerDisplayAllTableTextWithOutResults];
    self.displayAllTable.scrollEnabled = NO;
    self.places = [NSMutableArray array];
    self.placesTable.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    [self evaluateDisplayAllTable];
}


#pragma mark -
#pragma mark delegate SearchBar
/**
 *  Method that makes the table responsive
 *
 *  @param searchBar
 *  @param searchText
 */
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    [[DataManager shareDataManager] loadPlaces:searchText];
}

#pragma mark -
#pragma mark delegate DataManager
/**
 *  read places and populate placesTableView
 *
 *  @param places
 */
- (void)readPlaces:(NSMutableArray *)places{
    self.contentViewHeight.constant = 0;
    [self.places removeAllObjects];
    
    //check if array is not nil
    if(places){
        for(NSDictionary* place in places){
            DefaultPlace* newPlace = [DefaultPlace new];
            [newPlace parsePlaceFromNSDictionary:place];
            [self.places addObject:newPlace];
        }
    }
    [self evaluateDisplayAllTable];
    [self.placesTable reloadData];
}

#pragma mark -
#pragma mark delegate tableView

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return ViewControllerTableNumberOfSections;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(tableView == self.displayAllTable){
        return [self.displayAllTableArray count];
    }
    return [self.places count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = nil;
    
    if(tableView == self.displayAllTable){
        //displayAllTable
        cell = [tableView dequeueReusableCellWithIdentifier:ViewControllerDisplayAllTableCell forIndexPath:indexPath];
        cell.textLabel.text = [self.displayAllTableArray objectAtIndex:indexPath.row];
    }else{
        //placesTable
        cell = [tableView dequeueReusableCellWithIdentifier:ViewControllerPlacesCell forIndexPath:indexPath];
        DefaultPlace* place = [self.places objectAtIndex:indexPath.row];
        cell.textLabel.text = place.formatedAddress;
    }
    
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    return cell;
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    BOOL goToMap = TRUE;
    
    MapViewController* mapViewController = [self.storyboard instantiateViewControllerWithIdentifier:ViewControllerMapViewControllerIdentifier];
    if(tableView == self.placesTable){
        
        //unique marker for map
        DefaultPlace* selectedPlace = [self.places objectAtIndex:indexPath.row];
        mapViewController.mainPlace = selectedPlace;
        mapViewController.isUniquePlaceSelected = true;
        mapViewController.isPlaceAlreadyExists = [[DataManager shareDataManager] isPlaceAlreadyInDataBase:selectedPlace];
        
    }else{
        if([[self.displayAllTableArray firstObject] isEqualToString:ViewControllerDisplayAllTableTextWithResults]){
            //multiple markers for map
            mapViewController.selectedPlaces = self.places;
            mapViewController.isUniquePlaceSelected = false;
        }else{
            goToMap = NO;
        }
    }
    
    if(goToMap){
        [self.navigationController pushViewController:mapViewController animated:YES];
    }
}

#pragma mark -
#pragma mark private methods

/**
 *  evaluate if displayAllTable should be visible
 */
- (void)evaluateDisplayAllTable{
    switch ([self.places count]) {
        case 0:
            self.displayAllTableArray =  @[ViewControllerDisplayAllTableTextWithOutResults];
            self.contentViewHeight.constant = ViewControllerVisibleDisplayAll;
            break;
        case 1:
            self.contentViewHeight.constant = ViewControllerNotVisibleDisplayAll;
            break;
        default:
            self.displayAllTableArray =  @[ViewControllerDisplayAllTableTextWithResults];
            self.contentViewHeight.constant = ViewControllerVisibleDisplayAll;
            break;
    }
    
    //reload data for displayAllTable
    [self.displayAllTable reloadData];
}

@end
