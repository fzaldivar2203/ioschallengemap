//
//  DataManager.h
//  iOSChallengeMaps
//
//  Created by FUTAPP on 13/7/16.
//  Copyright © 2016 futapp. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DefaultPlace.h"
#import "Place.h"

@protocol DataManagerProtocol <NSObject>

@optional 
- (void)readPlaces:(NSMutableArray*) places;

@end

@interface DataManager : NSObject<DataManagerProtocol>

//Properties
@property(assign,nonatomic) id<DataManagerProtocol> delegate;

+(instancetype)shareDataManager;
- (void)loadPlaces: (NSString*) text;

-(BOOL)isPlaceAlreadyInDataBase:(DefaultPlace*) defaultPlace;
-(Place*)searchPlace:(DefaultPlace*) defaultPlace;

@end
