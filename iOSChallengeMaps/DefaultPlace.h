//
//  DefaultPlace.h
//  iOSChallengeMaps
//
//  Created by FUTAPP on 14/7/16.
//  Copyright © 2016 futapp. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DefaultPlace : NSObject

@property(nonatomic,strong) NSString* formatedAddress;
@property(nonatomic) double latitude;
@property(nonatomic) double longitude;

- (void)parsePlaceFromNSDictionary:(NSDictionary*) dictionary;

@end
