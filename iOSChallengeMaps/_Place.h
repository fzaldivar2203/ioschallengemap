// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Place.h instead.

#if __has_feature(modules)
    @import Foundation;
    @import CoreData;
#else
    #import <Foundation/Foundation.h>
    #import <CoreData/CoreData.h>
#endif

NS_ASSUME_NONNULL_BEGIN

@interface PlaceID : NSManagedObjectID {}
@end

@interface _Place : NSManagedObject
+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) PlaceID *objectID;

@property (nonatomic, strong, nullable) NSString* formatedAddress;

@property (nonatomic, strong, nullable) NSNumber* latitude;

@property (atomic) double latitudeValue;
- (double)latitudeValue;
- (void)setLatitudeValue:(double)value_;

@property (nonatomic, strong, nullable) NSNumber* longitude;

@property (atomic) double longitudeValue;
- (double)longitudeValue;
- (void)setLongitudeValue:(double)value_;

@end

@interface _Place (CoreDataGeneratedPrimitiveAccessors)

- (NSString*)primitiveFormatedAddress;
- (void)setPrimitiveFormatedAddress:(NSString*)value;

- (NSNumber*)primitiveLatitude;
- (void)setPrimitiveLatitude:(NSNumber*)value;

- (double)primitiveLatitudeValue;
- (void)setPrimitiveLatitudeValue:(double)value_;

- (NSNumber*)primitiveLongitude;
- (void)setPrimitiveLongitude:(NSNumber*)value;

- (double)primitiveLongitudeValue;
- (void)setPrimitiveLongitudeValue:(double)value_;

@end

@interface PlaceAttributes: NSObject 
+ (NSString *)formatedAddress;
+ (NSString *)latitude;
+ (NSString *)longitude;
@end

NS_ASSUME_NONNULL_END
