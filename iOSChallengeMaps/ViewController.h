//
//  ViewController.h
//  iOSChallengeMaps
//
//  Created by FUTAPP on 13/7/16.
//  Copyright © 2016 futapp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MagicalRecord/MagicalRecord.h>

@interface ViewController : UIViewController<UISearchBarDelegate, UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (weak, nonatomic) IBOutlet UITableView *displayAllTable;
@property (weak, nonatomic) IBOutlet UITableView *placesTable;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentViewHeight;

@property (strong,nonatomic)  NSArray* displayAllTableArray;
@property (strong,nonatomic)  NSMutableArray* places;


@end

