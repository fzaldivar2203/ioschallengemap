//
//  DefaultPlaceTest.m
//  iOSChallengeMaps
//
//  Created by FUTAPP on 14/7/16.
//  Copyright © 2016 futapp. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "DefaultPlace.h"

@interface DefaultPlaceTest : XCTestCase

@end

@interface DefaultPlace (Tests)

@end

@implementation DefaultPlaceTest

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}



- (void) testParse{

   //Create place using NSDictionary
    NSDictionary* location =[NSDictionary
                             dictionaryWithObjectsAndKeys:@"-23.4768588",@"lat",@"-46.8662018",@"lng",nil];
    NSDictionary* geometry =[NSDictionary
                             dictionaryWithObjectsAndKeys:location,@"location",nil];
    NSDictionary* dictionary = [NSDictionary
                            dictionaryWithObjectsAndKeys:@"Alphaville, Santana de Parnaíba - SP, Brasil",@"formatted_address",geometry,@"geometry",nil];
    DefaultPlace* defaultPlace = [DefaultPlace new];
                                 [defaultPlace parsePlaceFromNSDictionary:dictionary];
    
    //Create Place with values
    DefaultPlace* comparePlace = [DefaultPlace new];
    comparePlace.formatedAddress = @"Alphaville, Santana de Parnaíba - SP, Brasil";
    comparePlace.latitude = -23.4768588;
    comparePlace.longitude = -46.8662018;
    XCTAssertTrue([comparePlace.formatedAddress isEqual:defaultPlace.formatedAddress]
                  && (comparePlace.latitude == defaultPlace.latitude)
                   && (comparePlace.longitude == defaultPlace.longitude),@"Objects should be equal");
}



@end
